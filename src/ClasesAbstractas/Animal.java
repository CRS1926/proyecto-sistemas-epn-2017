package ClasesAbstractas;

public abstract class Animal {

	public String nombre;
	
	public Animal(){}
	
	void comer(){
		System.out.println("El"+nombre+"está comiendo");
	};
	
	public abstract void moverse();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	};

	
	
	
	
	
//	public static void main(String[] args) {
//		// TODO Apéndice de método generado automáticamente
//		
//		
//		
//
//	}

}
